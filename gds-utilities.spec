%define name 	gds-utilities
%define version 2.19.6
%define release 1.1
%define daswg   /usr
%define prefix  %{daswg}
%define dmtrun 1
%if %{dmtrun}
%define _sysconfdir /etc
%define config_runflag --enable-dmt-runtime --sysconfdir=%{_sysconfdir}
%else
%define config_runflag --disable-dmt-runtime
%endif
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

%define root_cling %(test "`root-config --has-cling`" != "yes"; echo $?)

Name: 		%{name}
Summary: 	gds-utilities 2.19.6
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc gcc-c++ glibc automake autoconf libtool m4 make
BuildRequires:  gzip bzip2 libXpm-devel
BuildRequires:  ldas-tools-framecpp >= 2.5.8
BuildRequires:  ldas-tools-framecpp-devel >= 2.5.8
BuildRequires:  ldas-tools-al-devel
BuildRequires:  root hdf5-devel
BuildRequires:  readline-devel fftw-devel
BuildRequires:  jsoncpp-devel
BuildRequires:  gds-dmt-devel >= 2.19.7
BuildRequires:  gds-base-devel >= 2.19.8
BuildRequires:  gds-frameio-devel >= 2.19.6
Requires:  gds-dmt >= 2.19.7
Requires:  gds-base >= 2.19.8
Requires:  gds-base >= 2.19.8
Requires:  gds-frameio >= 2.19.6
Prefix:		%prefix

%description
Global diagnostics software utilities

%package base
Summary: 	gds-utilities 2.19.6
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:  gds-dmt-base >= 2.19.7
Requires:  gds-base >= 2.19.8
Requires:  gds-frameio-base >= 2.19.6
Requires:  ldas-tools-framecpp

%description base
Global diagnostics software utilities


%package monitors
Summary: 	DMT Monitor programs
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-dmt-monitors >= 2.19.7
Requires: gds-dmt-base >= 2.19.7
Requires: gds-frameio-base >= 2.19.6
Requires: gds-services >= 2.19.8
Requires: jsoncpp-devel

%description monitors
GDS DMT monitor programs


%package web
Summary: 	DMT web services
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       gds-base >= 2.19.8

%description web
DMT web services

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  --prefix=%prefix --libdir=%{_libdir} \
	     --includedir=%{prefix}/include/gds \
	     --enable-online --enable-dtt %{config_runflag}
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files

%files base
%defattr(-,root,root)
%{_bindir}/when
%{_prefix}/etc/gds-user-env.*


%files monitors
%defattr(-,root,root)
%{_bindir}/CheckDataValid
%{_bindir}/FrWrite
%{_bindir}/mkcalibfile
%{_bindir}/MatchTrig
%{_bindir}/seg_calc
%{_bindir}/tablepgm

%changelog
* Fri Jan 28 2022 Edward Maros <ed.maros@ligo.org> - 2.19.6-1
- Updated for release as described in NEWS.md

* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- Updated for release as described in NEWS.md

* Tue Feb 23 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Removed references to gds-base-crtools package

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Removed python configuration
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Split lowlatency as a separate package
